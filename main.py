from time import time_ns
from functools import wraps


def performance(func):
    @wraps(func)
    def out(*args, **kwargs):
        begin = time_ns()
        result = func(*args, **kwargs)
        end = time_ns()
        print(f'Result is {result}. Calculated in: {end - begin} nanoseconds')
        return result

    return out


@performance
def raise_modulo_m(x, n, m):
    y = 1
    kw = x

    while (n > 0):
        lsb = n & 0b1
        if (lsb == 1):
            y = (y * kw) % m
        kw = (kw * kw) % m
        n = n >> 1
    return y


if __name__ == '__main__':
    try:
        x = int(input('x: '))
        n = int(input('n: '))
        m = int(input('m: '))
        result = raise_modulo_m(x, n, m)
        print(f"x={x}, n={n} (2^(10), m={m}")
    except Exception as e:
        print('Incorrect input: ' + str(e))
        raise e
