import unittest

import main


class TestPowerModuloM(unittest.TestCase):

    def test_calculate_power_modulo_m_k10(self):
        x = 5
        n = 1024
        m = 19
        print(f"x={x}, n={n} (2^(10), m={m}")
        result = main.raise_modulo_m(x, n, m)
        self.assertEqual(result, 16)

    def test_calculate_power_modulo_m_k20(self):
        x = 5
        n = 1048576
        m = 19
        print(f"x={x}, n={n} (2^(20), m={m}")
        result = main.raise_modulo_m(x, n, m)
        self.assertEqual(result, 17)

    def test_calculate_power_modulo_m_k50(self):
        x = 5
        n = 1125899906842624
        m = 19
        print(f"x={x}, n={n} (2^(50), m={m}")
        result = main.raise_modulo_m(x, n, m)
        self.assertEqual(result, 17)

    def test_calculate_power_modulo_m_k100(self):
        x = 5
        n = 1267650600228229401496703205376
        m = 19
        print(f"x={x}, n={n} (2^(100), m={m}")
        result = main.raise_modulo_m(x, n, m)
        self.assertEqual(result, 16)

if __name__ == '__main__':
    unittest.main()
